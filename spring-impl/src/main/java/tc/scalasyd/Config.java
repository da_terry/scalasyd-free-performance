package tc.scalasyd;

import javax.sql.DataSource;

import org.joda.time.LocalDate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories
public class Config {

  @Bean
  public DataSource dataSource() {
    HikariDataSource ds = new HikariDataSource();
    ds.setJdbcUrl("jdbc:postgresql://localhost:5432/scalasyd");
    ds.setUsername("postgres");
    ds.setPassword("postgres");
    ds.setMaximumPoolSize(50);
    return ds;
  }

  @Bean
  public JodaModule jacksonJodaModule() {
    JodaModule module = new JodaModule();
    module.addSerializer(LocalDate.class, new TimestampLocalDateSerializer());
    return module;
  }
  
}
