package tc.scalasyd;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

@Entity
@Table(name = "bands")
public class Band {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String name;
  
  @Column
  @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")  
  private LocalDate started;
  
  @OneToMany
  @JoinTable(
      name = "bands_artists", 
      joinColumns = @JoinColumn(name = "band_id"), 
      inverseJoinColumns = @JoinColumn(name = "artist_id")
    )
  private Collection<Artist> artists;

  protected Band() { /* JPA wants this */ }
  
  public Band(Long id, String name, LocalDate started, Collection<Artist> artists) {
    this.id = id;
    this.name = name;
    this.started = started;
    this.artists = artists;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public Long getId() {
    return id;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getName() {
    return name;
  }
  
  public void setStarted(LocalDate started) {
    this.started = started;
  }
  
  public LocalDate getStarted() {
    return started;
  }
  
  public void setArtists(Collection<Artist> artists) {
    this.artists = artists;
  }
  
  public Collection<Artist> getArtists() {
    return artists;
  }
}