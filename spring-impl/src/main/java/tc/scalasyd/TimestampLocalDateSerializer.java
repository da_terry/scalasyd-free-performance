package tc.scalasyd;

import java.io.IOException;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.joda.ser.JacksonJodaFormat;
import com.fasterxml.jackson.datatype.joda.ser.JodaDateSerializerBase;

public class TimestampLocalDateSerializer extends
    JodaDateSerializerBase<LocalDate> {

  protected final static JacksonJodaFormat DEFAULT_FORMAT = new JacksonJodaFormat(
      DEFAULT_DATEONLY_FORMAT);

  public TimestampLocalDateSerializer() {
    this(DEFAULT_FORMAT);
  }

  public TimestampLocalDateSerializer(JacksonJodaFormat format) {
    super(LocalDate.class, format);
  }

  @Override
  public TimestampLocalDateSerializer withFormat(JacksonJodaFormat formatter) {
    return (_format == formatter) ? this : new TimestampLocalDateSerializer(formatter);
  }

  @Override
  public void serialize(LocalDate value, JsonGenerator jgen,
      SerializerProvider provider) throws IOException, JsonGenerationException {
      jgen.writeNumber(value.toDate().getTime());
  }

  @Override
  public JsonNode getSchema(SerializerProvider provider,
      java.lang.reflect.Type typeHint) {
    return createSchemaNode(_useTimestamp(provider) ? "array" : "string", true);
  }
}