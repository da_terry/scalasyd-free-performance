package tc.scalasyd;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface BandRepository extends Repository<Band, Long> {
  
  @Query("SELECT DISTINCT b FROM Band b JOIN FETCH b.artists WHERE b.id < 11 ORDER BY b.id")
  public List<Band> findTop10();
  
//  public List<Band> findTop10() {
//    Query q = em.createQuery("SELECT b FROM Band b JOIN FETCH b.artists");
//    q.setMaxResults(10);
//    return q.getResultList();
//  }
  
  
}
