package tc.scalasyd;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
  
  @Autowired
  BandRepository bandRepository;
  
  @RequestMapping("/bands")
  public Collection<Band> bands() {
    return bandRepository.findTop10();
  }
}
