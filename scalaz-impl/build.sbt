name := "scalasyd-doobie"

version := "1.0"

scalaVersion := "2.11.6"

resolvers ++= Seq(
  "tpolecat" at "http://dl.bintray.com/tpolecat/maven",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

libraryDependencies ++= Seq(
  "org.tpolecat"            %%        "doobie-core"          % "0.2.2",
  "com.zaxxer"               %        "HikariCP"             % "2.2.5",
  "org.http4s"              %%        "http4s-blazeserver"   % "0.6.5",
  "org.http4s"              %%        "http4s-dsl"           % "0.6.5",
  "org.http4s"              %%        "http4s-argonaut"      % "0.6.5",
  "org.postgresql"           %        "postgresql"           % "9.2-1004-jdbc41",
  "joda-time"                %        "joda-time"            % "2.7",
  "io.argonaut"             %%        "argonaut"             % "6.1-M5",
  "org.slf4j"                %        "slf4j-simple"         % "1.7.7"
)

