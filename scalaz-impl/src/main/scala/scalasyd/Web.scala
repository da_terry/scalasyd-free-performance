package scalasyd

import org.http4s.server.HttpService
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.dsl._
import org.http4s.argonaut._
import org.http4s.EntityDecoder
import doobie.imports._
import argonaut._
import Argonaut._
import Json._

object Web {

  implicit val bandDecoder: EntityDecoder[Band] = jsonOf[Band]

  val service = HttpService {
    case GET -> Root / "bands" =>
      Ok {
        Database.findBandsWithArtists.transact(Database.tx).asJsonArray
      }
  }

  def main(args: Array[String]): Unit =
    BlazeBuilder.bindHttp(8080, "0.0.0.0")
      .mountService(service, "/")
      .run
      .awaitShutdown()

}

