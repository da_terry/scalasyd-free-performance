package scalasyd
package jdbc

import java.sql.DriverManager
import java.sql.ResultSet
import scalasyd.Database.BandArtistJoin
import org.joda.time.LocalDate
import scalaz.concurrent.Task

object RawJdbcQuery {

  val conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/scalasyd", "postgres", "postgres")

  def readN(i: Int): Unit =
    findBandsWithArtists(i).map { rs =>
      try while (rs.next) {
        toEntity(rs)
      }
      finally rs.close
    }.run

  def findBandsWithArtists(i: Int): Task[ResultSet] =
    Task.delay {
      conn.prepareStatement(s"""
        SELECT b.id, b.name, b.started, a.id, a.name
        FROM bands b
        INNER JOIN bands_artists ba ON (b.id = ba.band_id)
        INNER JOIN artists a ON (a.id = ba.artist_id)
        LIMIT $i
      """).executeQuery
    }

  def toEntity(rs: ResultSet): BandArtistJoin =
    BandArtistJoin(
      BandId(rs.getLong(1)),
      Band(rs.getString(2), new LocalDate(rs.getDate(3).getTime)),
      ArtistId(rs.getLong(4)),
      Artist(rs.getString(5))
    )

  def test(i: Int): Unit = {
    val s = System.nanoTime
    readN(i)
    println(s"jdbc read [$i] took ${(System.nanoTime - s)/1000000} ms")
  }

  def main(args: Array[String]): Unit = {
    // warmup
    println("warmup")
    for (_ <- 1 to 5) readN(50000)
    println("warmup done")

    for (_ <- 1 to 5) test(1)
    for (_ <- 1 to 5) test(10)
    for (_ <- 1 to 5) test(100)
    for (_ <- 1 to 5) test(1000)
    for (_ <- 1 to 5) test(10000)
  }
}