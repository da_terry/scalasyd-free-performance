package scalasyd
package jdbc

import scalaz.stream._
import doobie.imports._
import Database._
import scalaz.concurrent.Task
import scalasyd.Database

object DoobieQuery {

  val tx = DriverManagerTransactor[Task]("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/scalasyd", "postgres", "postgres")

  def readN(i: Int): Unit =
    findBandsWithArtists(i).transact(tx).run.run

  def findBandsWithArtists(i: Int): Process[ConnectionIO, BandArtistJoin] =
    sql"""
      SELECT b.id, b.name, b.started, a.id, a.name
      FROM bands b
      INNER JOIN bands_artists ba ON (b.id = ba.band_id)
      INNER JOIN artists a ON (a.id = ba.artist_id)
      LIMIT $i
    """.query[BandArtistJoin].process

  def test(i: Int): Unit = {
    val s = System.nanoTime
    readN(i)
    println(s"doobie read [$i] took ${(System.nanoTime - s)/1000000} ms")
  }

  def main(args: Array[String]): Unit = {
    println("warmup")
    for (_ <- 1 to 5) readN(50000)
    println("warmup done")

    for (_ <- 1 to 5) test(1)
    for (_ <- 1 to 5) test(10)
    for (_ <- 1 to 5) test(100)
    for (_ <- 1 to 5) test(1000)
    for (_ <- 1 to 5) test(10000)
  }
}
