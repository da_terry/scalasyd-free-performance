package scalasyd

import doobie.imports._
import scalaz._
import Scalaz._
import scalaz.concurrent.Task
import scalaz.stream._
import java.sql.{ Date => SqlDate }
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import Json._
import javax.sql.DataSource
import com.zaxxer.hikari.HikariDataSource

object Database {

  val tx: Transactor[Task] =
    DataSourceTransactor[Task] {
      new HikariDataSource() <| { ds =>
        ds.setJdbcUrl("jdbc:postgresql://localhost:5432/scalasyd")
        ds.setUsername("postgres")
        ds.setPassword("postgres")
        ds.setMaximumPoolSize(50)
      }
    }

  implicit val localDateMeta: Meta[LocalDate] =
    Meta[SqlDate].xmap(
      s => new LocalDate(s.getTime),
      l => new SqlDate(l.toDateTimeAtStartOfDay(DateTimeZone.UTC).getMillis))

  case class BandArtistJoin(bid: BandId, b: Band, aid: ArtistId, a: Artist)

  def findBandsWithArtists =
    sql"""
      SELECT b.id, b.name, b.started, a.id, a.name
      FROM bands b
      INNER JOIN bands_artists ba ON (b.id = ba.band_id)
      INNER JOIN artists a ON (a.id = ba.artist_id)
      WHERE b.id < 11
      ORDER BY b.id
    """.query[BandArtistJoin].process.chunkBy2 { _.bid == _.bid }.map(groupToBands)

  def groupToBands(bas: Vector[BandArtistJoin]): BandArtistView =
    BandArtistView(
      BandView(bas.head.bid, bas.head.b),
      bas.map { b =>
        ArtistView(b.aid, b.a)
      })
}