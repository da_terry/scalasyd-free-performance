package scalasyd

import org.joda.time.LocalDate

case class BandId(id: Long)
case class Band(name: String, started: LocalDate)

case class ArtistId(id: Long)
case class Artist(name: String)

case class ArtistView(id: ArtistId, artist: Artist)
case class BandView(id: BandId, band: Band)

case class BandArtistView(band: BandView, artists: Vector[ArtistView])
